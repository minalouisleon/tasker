/** @type {import('next').NextConfig} */
const nextConfig = {
    output: 'export',
    transpilePackages: [
        "@ionic/react",
        "@ionic/core",
        '@stencil/core',
        'ionicons',
    ]
};

export default nextConfig;
