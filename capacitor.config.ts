import type { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.tasker.minalouis.app',
  appName: 'tasker',
  webDir: 'out',
  server : {
    url : "http://192.168.x.x:3000",
    cleartext: true
  }
};

export default config;
