import Link from "next/link"

export default function Home() {
  return (
    <main>
      <Link href="/mobile/home" >mobile</Link>
    </main>
  );
}
